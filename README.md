# README #

## Django Test ##

### The Task ###

Create a Django application for storing and serving data about NASA facilities.

Facilities data is published by NASA via an API; https://data.nasa.gov/Management-Operations/NASA-Facilities/gvk9-iz74

The data should be able to be imported into a database. Existing facilities do not need to be updated but newly added facilities should be imported to the application.

Admin users should be able to edit existing facilities, but not delete them or add new ones.

The application should provide an API endpoint to serve the facilities data in JSON format. Users do not need to be authenticated to access the API endpoint and only �active� facilities should be included.

### Submission ###

Any installations required for libraries etc. should be done in a Python virtual environment. All required setup, including virtual environments, databases etc., should be achieved via a single script; setup.sh

The application should be built to run on Ubuntu 16.04.

This task shouldn�t take much more than 4 hours. Please submit your code in a zip file or a by a link to a publicly accessible URL.

## My Submission ##

### System Requirements ###

This application was created on Ubuntu 17.10 and has been tested on an Ubuntu 16.04 boot USB. A working python3 installation, including virtualenv and pip, is required.

### Installation ###

After cloning the repository cd into the repositorie's upper most directory and run the command: 

`bash startup.sh`

This will set up a virtual environment, install all requirements, make migrations, and run some test scripts.
It will then create a super user, the user will be asked to create a username and password.
It will then run the django test server, which should direct the user to the appropriate IP address to type in to their browser.

### Using ###

Go to the appropriate IP address (probably 127.0.0.1:8000).

You will be presented with a list of possible URLs, the first one is simply '/all/', click on that and the data for all 
'Active' NASA facilities will be presented in JSON format. The other URLs on the index page are simply ways to select a 
subset of the data.

Go to /admin/ to access the admin control panel (you will need to log in with your user name and password), click on Nasa_facilitys
to see all of the nasa facility objects, including ones with a non-ctive status. Notice that you can edit the facilities (including
making them Active or Inactive) but you cannot delete them, or create a new one.



