#!/usr/bin/env bash
# startup.sh
# A bash script to install the AstroSatEcxercise system.

# Step one. Create the virtualenv and activate it
virtualenv -p python3 env_astro
source env_astro/bin/activate

# Step two. Install all python modules.
pip install -r requirements.txt

# Step three. Make migrations.
python astro_project/manage.py makemigrations
python astro_project/manage.py migrate

# Step four. Download and update the data.
python astro_project/downloadNasaFacilities.py

# Step five. Run unit tests.
python astro_project/manage.py test nasa_facilities

# Step six. Create a super user. This will request input from the user.
python astro_project/manage.py createsuperuser

# Step seven. Run the test server.
python astro_project/manage.py runserver