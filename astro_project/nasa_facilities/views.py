import pandas as pd
from datetime import datetime

from django.http import HttpResponse, Http404
from django.shortcuts import render

from nasa_facilities.models import nasa_facility
# Create your views here.

def index(request):
  """
  Return a list of possible urls for state, center and facility.
  """

  objects = nasa_facility.objects.filter(status='Active').values()
  df = pd.DataFrame.from_records(objects)
  # Get all states.
  states = sorted(list(df['state'].unique()))
  statelist = [{'url': x, 'name': x} for x in states]
  # Get all centers.
  centers = sorted(list(df['center'].unique()))
  centerlist = [{'url': x.replace(' ', '_-ws-_'), 'name': x} for x in centers]
  # Get all facilities.
  facilities = sorted(list(df['facility'].unique()))
  facilitylist = [{'url': x.replace(' ', '_-ws-_').replace('/', '_-fs-_'), 'name': x} for x in facilities]
  # See * comment below.

  context = {'statelist': statelist,
             'centerlist': centerlist,
             'facilitylist': facilitylist}
  return render(request, 'nasa_facilities/index.html', context)


  return HttpResponse("<a href='data.json'>Data</a>")

def api(request, name):
  """
  Return the objects, in json format, of any suitable facility.

  If name is 'all' then all Active objects will be returned.

  If name is either a state abbreviation, a center name, or a facility name
  then the returned data will be filtered appropriately.

  If name is not understood then a 404 error will be raised. Future work could
  show the facility list as part of the 404 error.
  """

  name_ = name.replace('_-fs-_', '/').replace('_-ws-_', ' ')
  # *
  # replace '_-ws-_' with space becuse the facility and center names usually
  # have spaces, but I want my urls to have underscores.
  # Also replace '_-fs-_' with '/' because some facilities have a '/' in their name, but the
  # urls have been replaced with '_-fs-_'.
  # This will fail if any facilities or centers actually do have an underscore
  # in their name.
  # The repeated replace calls is a bit ugly anyway. It would be better to add a 'url_name'
  # property to the model that deals with these complications directly.
  
  if name == 'all':
    # Get all active objects.
    objects = list(nasa_facility.objects.filter(status='Active').values())
  else:
    # It could be a state, a center, or a facility.
    gotItems = 0
    for tryfield in ['state', 'center', 'facility']:
      kwargs = {'status': 'Active',
                '{0}'.format(tryfield): name_}
      objects = list(nasa_facility.objects.filter(**kwargs).values())
      gotItems = len(objects)
      if gotItems > 0:
        break
    if gotItems == 0:
      raise Http404("No NASA facilities matching string '{}'.".format(name_))

  # Convert to a pandas dataframe to make datetime conversion easier
  df = pd.DataFrame(objects)

  # Convert datetimes to strings
  dtCols = ['last_update', 'occupied', 'record_date']
  for rowi, row in df.iterrows():
    for dtCol in dtCols:
      try:
        df.ix[rowi, dtCol] = datetime.strftime(df.ix[rowi, dtCol], '%Y-%m-%dT%H:%M:%S.%f')
      except (TypeError, ValueError):
        # Happens when the datetime is "nan" or empty.
        df.ix[rowi, dtCol] = ''

  # Drop my own rows.
  dropcols = ['model_import', 'user_update']
  df = df.drop(dropcols, axis=1)

  # Convert to json
  json_data = df.to_json(orient='records')

  # Serve with HttpResponse.
  # JsonResponse want s a dict input, so only works if serving a single object.
  return HttpResponse(json_data, content_type='application/json')


