from django.db import models
from datetime import datetime

# Create your models here.
class nasa_facility(models.Model):
  """
  The nasa facility model.
  From the API at https://data.nasa.gov/Management-Operations/NASA-Facilities/gvk9-iz74
  all fields except the coordinates are provided as strings (the coordinates
  are floats). I will save the fields in whatever type they appear to be, error
  handling should catch any issues.

  Ignoring the Zip Code field,
  """

  statusChoices = (('Inactive', 'Inactive'),
                   ('Active', 'Active'),
                   ('Under Rehabilitation', 'Under Rehabilitation'),
                   ('Under', 'Under'))

  # Generic details
  facility = models.CharField(max_length=100, unique=True)
  center = models.CharField(max_length=35)
  center_search_status = models.CharField(max_length=10)
  city = models.CharField(max_length=25)
  state = models.CharField(max_length=2)
  country = models.CharField(max_length=5)
  contact = models.CharField(max_length=35)
  phone = models.CharField(max_length=12)
  url_link = models.CharField(max_length=120)
  status = models.CharField(max_length=20, null=True, choices=statusChoices)
  computed_region_cbhk_fwbd = models.IntegerField()
  computed_region_nnqa_25f4 = models.IntegerField()

  # Datetime fields from source.
  last_update = models.DateTimeField(null=True)
  occupied = models.DateTimeField(null=True)
  record_date = models.DateTimeField()

  # Datetime fields own.
  model_import = models.DateTimeField()
  user_update = models.DateTimeField()

  # Location details
  location_type = models.CharField(max_length=5)
  location_zip = models.CharField(max_length=10)
  location_lon = models.FloatField()
  location_lat = models.FloatField()

  def save(self, *args, **kwargs):
    ''' On save, update timestamps '''
    if not self.id:
      self.model_import = datetime.now()
    self.user_update = datetime.now()
    return super(nasa_facility, self).save(*args, **kwargs)

  def __str__(self):
    return self.facility

