from django.apps import AppConfig


class NasaFacilitiesConfig(AppConfig):
    name = 'nasa_facilities'
