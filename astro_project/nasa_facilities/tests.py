import unittest
from django.test import Client
import json

# Create your tests here.
from .models import nasa_facility

class nasa_facility_tests(unittest.TestCase):
  """
  A few client tests to ensure that pages are found etc.
  """

  def setUp(self):
    self.client = Client()
    # Destroy all objects first. For some reason these objects are persisting
    # between test runs.
    nasa_facility.objects.all().delete()
    # Set up a few entries
    nasa_facility.objects.create(center="Kennedy Space Center",
                                 center_search_status	="Public",
                                 city="Kennedy Space Center",
                                 computed_region_cbhk_fwbd=30,
                                 computed_region_nnqa_25f4=1078,
                                 contact="Sheryl Chaffee",
                                 country="US",
                                 facility="Corrosion Test Site, K8-0237",
                                 last_update="2015-06-22T00:00:00.000000",
                                 location_lat=28.538331,
                                 location_lon=-81.378879,
                                 location_type=	"Point",
                                 location_zip="32899",
                                 occupied="2000-01-01T00:00:00.000000",
                                 phone="321-867-8047",
                                 record_date="1998-05-11T00:00:00.000000",
                                 state="FL",
                                 status="Active",
                                 url_link="nan")
    nasa_facility.objects.create(center="Eddy Space Center",
                                 center_search_status	="Public",
                                 city="Eddy City",
                                 computed_region_cbhk_fwbd=30,
                                 computed_region_nnqa_25f4=1078,
                                 contact="Sheryl Chaffee",
                                 country="US",
                                 facility="Blah Blah facility 3",
                                 last_update="2015-06-22T00:00:00.000000",
                                 location_lat=28.538331,
                                 location_lon=-81.378879,
                                 location_type=	"Point",
                                 location_zip="32899",
                                 occupied="2000-01-01T00:00:00.000000",
                                 phone="321-867-8047",
                                 record_date="1998-05-11T00:00:00.000000",
                                 state="CO",
                                 status="Active",
                                 url_link="nan")
    nasa_facility.objects.create(center="Eddy Space Center",
                                 center_search_status	="Public",
                                 city="Eddy City",
                                 computed_region_cbhk_fwbd=30,
                                 computed_region_nnqa_25f4=1078,
                                 contact="Sheryl Chaffee",
                                 country="US",
                                 facility="Blah Blah facility/4",
                                 last_update="2015-06-22T00:00:00.000000",
                                 location_lat=28.538331,
                                 location_lon=-81.378879,
                                 location_type=	"Point",
                                 location_zip="32899",
                                 occupied="2000-01-01T00:00:00.000000",
                                 phone="321-867-8047",
                                 record_date="1998-05-11T00:00:00.000000",
                                 state="CO",
                                 status="Active",
                                 url_link="nan")
    nasa_facility.objects.create(center="Kennedy Space Center",
                                 center_search_status	="Public",
                                 city="Kennedy Space Center",
                                 computed_region_cbhk_fwbd=30,
                                 computed_region_nnqa_25f4=1078,
                                 contact="Sheryl Chaffee",
                                 country="US",
                                 facility="Blah Blah facility 2",
                                 last_update="2015-06-22T00:00:00.000000",
                                 location_lat=28.538331,
                                 location_lon=-81.378879,
                                 location_type=	"Point",
                                 location_zip="32899",
                                 occupied="2000-01-01T00:00:00.000000",
                                 phone="321-867-8047",
                                 record_date="1998-05-11T00:00:00.000000",
                                 state="FL",
                                 status="Active",
                                 url_link="nan")
    nasa_facility.objects.create(center="Kennedy Space Center",
                                 center_search_status	="Public",
                                 city="Kennedy Space Center",
                                 computed_region_cbhk_fwbd=30,
                                 computed_region_nnqa_25f4=1078,
                                 contact="Sheryl Chaffee",
                                 country="US",
                                 facility="Blah blah InActive",
                                 last_update="2015-06-22T00:00:00.000000",
                                 location_lat=28.538331,
                                 location_lon=-81.378879,
                                 location_type=	"Point",
                                 location_zip="32899",
                                 occupied="2000-01-01T00:00:00.000000",
                                 phone="321-867-8047",
                                 record_date="1998-05-11T00:00:00.000000",
                                 state="FL",
                                 status="Inactive",
                                 url_link="nan")

  def test_allReturnsMultiJSON(self):
    response = self.client.get('/all')
    # Check that the response is 200 OK.
    self.assertEqual(response.status_code, 200)
    # Check that the response is json data.
    self.assertTrue('"application/json"' in str(response.json))
    # Check that there are multiple results
    data = json.loads(response.content.decode('utf-8'))
    self.assertGreater(len(data), 1)
    # Check that only 'Active' facilities are output.
    for obj in data:
      self.assertTrue(obj['status'] == 'Active')

  def test_activeFacilityReturnsSingleJSON(self):
    name = 'Blah Blah facility 2'
    name_ = name.replace(' ', '_-ws-_').replace('/', '_-fs-_')
    response = self.client.get('/{}'.format(name_))
    # Check that the response is 200 OK.
    self.assertEqual(response.status_code, 200)
    # Check that the response is json data.
    self.assertTrue('"application/json"' in str(response.json))
    # Check that there is only a single object.
    data = json.loads(response.content.decode('utf-8'))
    self.assertEqual(len(data), 1)

  def test_activeFacilityWithSpaceInName(self):
    name = 'Blah Blah facility 3'
    name_ = name.replace(' ', '_-ws-_').replace('/', '_-fs-_')
    response = self.client.get('/{}'.format(name_))
    # Check that the response is 200 OK.
    self.assertEqual(response.status_code, 200)
    # Check that the response is json data.
    self.assertTrue('"application/json"' in str(response.json))
    # Check that there is only a single object.
    data = json.loads(response.content.decode('utf-8'))
    self.assertEqual(len(data), 1)


  def test_activeFacilityWithSlashInName(self):
    name = 'Blah Blah facility/4'
    name_ = name.replace(' ', '_-ws-_').replace('/', '_-fs-_')
    response = self.client.get('/{}'.format(name_))
    # Check that the response is 200 OK.
    self.assertEqual(response.status_code, 200)
    # Check that the response is json data.
    self.assertTrue('"application/json"' in str(response.json))
    # Check that there is only a single object.
    data = json.loads(response.content.decode('utf-8'))
    self.assertEqual(len(data), 1)


  def test_innactiveFacilityReturns404(self):
    name = 'Blah blah InActive'
    name_ = name.replace(' ', '_-ws-_').replace('/', '_-fs-_')
    response = self.client.get('/{}'.format(name_))
    self.assertEqual(response.status_code, 404)

  def test_randomNameReturns404(self):
    response = self.client.get('/kljkjghgkjl')
    self.assertEqual(response.status_code, 404)
