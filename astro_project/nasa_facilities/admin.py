from django.contrib import admin
from .models import nasa_facility

# Register your models here.

class nasa_facilityAdmin(admin.ModelAdmin):
  list_display = ('facility', 'center', 'state', 'status')
  ordering = ('facility',)
  search_fields = ('facility', 'state', 'status')


  def has_delete_permission(self, request, obj=None):
    # Cannot delete a facility.
    return False

  def has_add_permission(self, request):
    # Cannot create a facility.
    return False

  def get_actions(self, request):
    # Disable delete from the dropdown. It wouldn't work because delete has
    # been disabled, but for completeness...
    actions = super(nasa_facilityAdmin, self).get_actions(request)
    del actions['delete_selected']
    return actions

admin.site.register(nasa_facility, nasa_facilityAdmin)