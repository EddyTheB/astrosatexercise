#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 16:09:45 2018

Download data from the NASA Facilities website at
https://data.nasa.gov/Management-Operations/NASA-Facilities/gvk9-iz74.

Actually I will use the API URL at
https://data.nasa.gov/resource/9g7e-7hzz.json

@author: eddy
"""
import os

import argparse
from datetime import datetime
import urllib.request
import json
import pandas as pd
from numpy import isnan

import django
os.environ["DJANGO_SETTINGS_MODULE"] = 'astro_project.settings'
django.setup()

from nasa_facilities.models import nasa_facility


DefaultURL = 'https://data.nasa.gov/resource/9g7e-7hzz.json'

def downloadJSON(URL=DefaultURL):
  response = urllib.request.urlopen(URL)
  content = response.read().decode('utf8')
  data = json.loads(content)
  return data

def extractLocation(df):
  """
  Extract the location details form the data frame.
  I need to extract the location type, and each of two coordinates, so I'll do
  a single for loop instead of three .apply loops.
  """
  df['location_type'] = None
  df['location_lon'] = None
  df['location_lat'] = None
  for ri, row in df.iterrows():
    df.ix[ri, 'location_type'] = row['location']['type']
    df.ix[ri, 'location_lon'] = row['location']['coordinates'][0]
    df.ix[ri, 'location_lat'] = row['location']['coordinates'][1]
    if len(row['location']['coordinates']) > 2:
      # raise an error if there is more than one coordinate, just in case some
      # features are described as polygons, or something else.
      print(row)
      print(row['location'])
      raise ValueError('Location has more than two coordinates.')
  df = df.drop('location', axis=1)
  return df

def JSON2DataFrame(jsondata):
  # Convert the json data into a pandas dataframe.
  df = pd.DataFrame(jsondata)
  df = extractLocation(df)
  return df

def printDetails(df):
  """
  Print details for each column in the dataframe.

  This is to assist in preparing the model parameters.

  Currently only a few details could extend to whether strings are all
  datelike, or numeric, sometimes Null, or whatever.
  """
  print('Num of rows: {}'.format(len(df.index)))
  colNames = list(df)
  for colName in colNames:
    unique = pd.unique(df[colName])
    print("{} - type: {} longest_str: {} numUnique: {} firstValue: {} ".format(
              colName,
              data[colName].dtype,
              df[colName].str.len().max(),
              len(unique),
              df.ix[0, colName],))
    if len(unique) < 10:
      for ui, u in enumerate(unique):
        print('  {}: {}'.format(ui, u))

  df['ZipsEqual'] = df.apply(lambda row: 1 if row['zipcode'] == row['location_zip'] else 0,axis=1)
  if df['ZipsEqual'].min() == 1:
    print('Columns zipcode and location_zip are always identical.')

def convertDateTime(datestr):
  """
  Convert a datetime from the string format used in the api to the format
  required by the django model.
  """
  try:
    dt = datetime.strptime(datestr, '%Y-%m-%dT%H:%M:%S.%f')
  except TypeError:
    if isnan(datestr):
      return None
    else:
      print(datestr)
      raise
  return dt

def logFacilities(df):
  """
  Send each row in turn to the model.
  Use get_or_create so that if a model object with the same facility name
  already exists then an identical one will not be created.
  """
  for rowi, row in df.iterrows():

    obj, created = nasa_facility.objects.get_or_create(
        facility=row.facility,              # facility is the unique identifyer to see if it already exists.
        defaults={'center': row.center,
                  'center_search_status': row.center_search_status,
                  'city': row.city,
                  'state': row.state,
                  'country': row.country,
                  'contact': row.contact,
                  'phone': row.phone,
                  'url_link': row.url_link,
                  'status': row.status,
                  'computed_region_cbhk_fwbd': row[':@computed_region_cbhk_fwbd'],
                  'computed_region_nnqa_25f4': row[':@computed_region_nnqa_25f4'],
                  'last_update': convertDateTime(row.last_update),
                  'occupied': convertDateTime(row.occupied),
                  'record_date': convertDateTime(row.record_date),
                  'location_type': row.location_type,
                  'location_zip': row.location_zip,
                  'location_lon': row.location_lon,
                  'location_lat': row.location_lat})
    if created:
      print('Created new item: {}'.format(row.facility))
    else:
      print('Skipped existing item: {}'.format(row.facility))

if __name__ == '__main__':

  argDesc = ("Downloads the data from a json web api (tested and designed for "
             "{}) and uploads the results to a Django model. "
             "Could be run as a cron script to continually update the database. "
             "Note that this script will NOT update any existing objects in the model.").format(DefaultURL)

  parser = argparse.ArgumentParser(description=argDesc)
  parser.add_argument('-u', metavar='url', type=str,
                      nargs='?', default=DefaultURL,
                      help=("The URL from which to grab the json data. Default "
                            "'{}'.").format(DefaultURL))
  parser.add_argument('-m', metavar='mode', type=str,
                      nargs='?', default='extractUpdated',
                      choices=['extract', 'printDetails'],
                      help=("The operation mode, one of 'extract', "
                            "'printDetails'. 'printDetails' will print some "
                            "details about each item in the json (useful "
                            "mainly for deciding on the model properties). "
                            "'extract' will extract all data and add it to the "
                            "model (and therefore the database). Defaulat 'extract'."))
  args = parser.parse_args()

  data = downloadJSON(URL=args.u)
  data = JSON2DataFrame(data)
  if args.m == 'printDetails':
    printDetails(data)
  else:
    # Must be extract
    logFacilities(data)


